/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.utils.excel.fieldtype;

import com.empire.emsite.modules.sys.entity.Office;

/**
 * 类OfficeType.java的实现描述：字段类型转换[ TODO 该类需要重写]
 * 
 * @author arron 2017年10月30日 下午1:08:47
 */
public class OfficeType {

    /**
     * 获取对象值（导入）[ TODO 该方法需要重写]
     */
    public static Object getValue(String val) {
        return null;
    }

    /**
     * 设置对象值（导出）
     */
    public static String setValue(Object val) {
        if (val != null && ((Office) val).getName() != null) {
            return ((Office) val).getName();
        }
        return "";
    }
}
